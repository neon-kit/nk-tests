
# Add library include folder
CC_INCLUDES += ../unit-testing-framework/source
CC_INCLUDES += ../unit-testing-framework/external

AR_LIBS += $(WS)/../unit-testing-framework/generated/unit-testing-framework.a

$(WS)/../unit-testing-framework/generated/unit-testing-framework.a:
	$(MAKE) -C . -f library.Makefile
